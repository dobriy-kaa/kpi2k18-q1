const jss = require('jss');
const loaderUtils = require('loader-utils');
var serialize = require('serialize-javascript');

//plugins from 'jss-preset-default'
const plugins = [
  require("jss-camel-case"),
  require("jss-compose"),
  require("jss-default-unit"),
  require("jss-expand"),
  require("jss-extend"),
  require("jss-global"),
  require("jss-nested"),
  require("jss-props-sort"),
  require("jss-template"),
  require("jss-vendor-prefixer")
]

module.exports = function(source) {
  let shiet, rules;
  if (this.inputValue) {
    return null, this.inputValue;
  } else {
    shiet = jss.create();

    plugins.forEach(function(plugin) {
      shiet = shiet.use(plugin.default());
    });

    rules = this.exec(source, this.resource);

    console.log('\n------start source-----\n')
    console.log(source);
    console.log('\n------end source-----\n')
    console.log('\n------start rules-----\n')
    console.log(rules);
    console.log('\n------end rules-----\n')

    return shiet
      .createStyleSheet(rules)
      .toString()
  }

};
