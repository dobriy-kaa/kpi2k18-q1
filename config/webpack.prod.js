const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const ExtractJssPlugin = require("extract-jss-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

const cssLoader = new ExtractTextWebpackPlugin({
  filename: "[name].[contenthash].css",
});

const jssLoader = new ExtractTextWebpackPlugin('jssStyles.css'); 

module.exports = {
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss/,
        use: cssLoader.extract({
          use: [
            {
              loader:  'css-loader',
              options: {
                modules:         true,
                localIdentName:  '[path][local]_[hash:base64:3]',
                minimize:        false,
                discardComments: {removeAll: true},
              },
            },
            {
              loader:  'sass-loader',
              options: {
                includePaths: [
                  'src/scss',
                ],
              }
            }
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.js$/,
        include: /(src\/jss)/,
        use: jssLoader.extract({
          use: ['css-loader', path.resolve(__dirname, '../jssExtractLoader.js')],
          fallback: 'style-loader'
        })
      },
    ]
  },
  plugins: [
    cssLoader, 
    jssLoader,
    new UglifyJSPlugin({
      sourceMap: true
    })
  ]
}