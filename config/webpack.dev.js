const config =  {
  devtool: "eval-source-map",
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader:  'css-loader',
            options: {
              modules:         true,
              localIdentName:  '[path][local]_[hash:base64:3]',
              minimize:        false,
              discardComments: {removeAll: true},
            },
          },
          {
            loader:  'sass-loader',
            options: {
              includePaths: [
                'src/scss',
              ],
            }
          },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
    ]
  }
}


module.exports = config;