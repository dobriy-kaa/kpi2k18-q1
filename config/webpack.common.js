const path = require('path')
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const jssLoader = require('jss-module-loader');

const config =  {
  entry: path.resolve(__dirname, "../", "src/app.js"),
  output: {
    filename: "[hash].bundle.js",
    path: path.resolve(__dirname, "../", "dist")
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/(node_modules)/, path.resolve(__dirname, '../src/jss')],
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: ['transform-runtime'],
              presets: ['stage-0', 'react']
            }
          }
        ]
      },
      {
        test: /\.jp(e?)g$/,
        use: [
          'url-loader'
        ]
      },
    ]
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ],
  resolve: {
    modules: [path.resolve(__dirname, '../src'), 'node_modules'],
  },
}


module.exports = config;