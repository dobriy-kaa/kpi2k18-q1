const webpackMerge = require('webpack-merge');
const commonConfig = require('./config/webpack.common');

const addons = (addonsArg) => {
  let addons = []
    .concat.apply([], [addonsArg])
    .filter(Boolean)

  return addons.map((addonName) => require(`./config/addons/webpack.${addonName}.js`))
}

const DEFAULT_ENV = {
  env: 'dev'
}

module.exports = (env = DEFAULT_ENV) => {
  const envConfig = require(`./config/webpack.${env.env}.js`)
  return webpackMerge(commonConfig, envConfig, ...addons(env.addons));
};