import React from 'react';
import Chance from 'chance';
import { render } from 'react-dom';
import Search from './components/Search';
import UsersList from './components/UsersList';
import {ApplyTheme, createJss, createSheetsRegistry} from 'rambler-ui/theme';
import jssStyles from './jss/jss-example';
import s from './app.scss';

const chance = new Chance();

const userTemplate = (obj) => ({
  email: obj.email,
  avatarUrl: obj.avatarUrl,
  name: obj.name,
  id: obj.id
})
const USERS_QNT = 1000;
const LIMIT = 12;
const users = Array.apply(null, Array(USERS_QNT)).map((item, index) => {
  return userTemplate({
    email: chance.email(),
    avatarUrl: chance.avatar(),
    name: chance.name(),
    id: chance.natural()
  })
})

const getUser = (id) => {
  const foundUser = users.filter(item => item.id === id);
  if (!foundUser.length) {
    return null;
  }
  return foundUser[0];
}

class App extends React.Component {
  state = {
    result: [],
    query: '',
    nextPage: 0
  }

  componentDidMount() {
    this.filterUsers();
  }
  
  filterUsers(query, page = 1) {
    const start = LIMIT * (page-1);
    const end = LIMIT * page;

    let newState = {};

    if (!query) {
      console.log('if !query', query, start, end)
      newState.result = users.slice(start, end);
      if (LIMIT * page < users.length) {
        newState.nextPage = page + 1;
      } else {
        newState.nextPage = 0;
      }
      this.setState(newState)
      return false;
    }

    const newUsers =  users.filter((item) => 
      item.email.includes(query) || item.name.includes(query)
    );
    newState.result = newUsers.slice(start, end);
    if (LIMIT * page < newUsers.length) {
      newState.nextPage = page + 1;
    } else {
      newState.nextPage = 0;
    }
    if (newState.result.length) {
      this.setState(newState);
    }
  }

  loadMore = () => {
    const {
      nextPage,
      query
    } = this.state;
    console.log('loadmore', nextPage)
    this.filterUsers(query, nextPage);
  }

  onSearchInput = (query) => {
    this.setState({query});
    this.filterUsers(query);
  }

  render() {
    const {
      result,
      nextPage
    } = this.state;

    const props = {
      users: result
    };
    if (nextPage) {
      props.loadMore = this.loadMore
    }

    return (
      <ApplyTheme>
        <div className={s.root}>
          <Search
            onFilterQueryChange={this.onSearchInput}
          />
          <UsersList
            {...props}
          />
        </div>
      </ApplyTheme>
    );
  }
}

render(<App />, document.getElementById('app'));