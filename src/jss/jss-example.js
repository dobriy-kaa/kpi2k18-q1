//our little jss example
module.exports = {
  button: {
    fontSize: 12,
    '&:hover': {
      background: 'blue'
    }
  },
  ctaButton: {
    extend: 'button',
    '&:hover': {
      background: 'deepblue'
    }
  },
  '@media (min-width: 1024px)': {
    button: {
      width: 200
    }
  }
}