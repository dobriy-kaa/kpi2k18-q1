import React from 'react';
import * as pt from 'prop-types';
import s from './index.scss';

class Search extends React.Component {
  static propTypes = {
    onFilterQueryChange: pt.func.isRequired
  }

  static defaultProps = {
    users: []
  }

  onChange = () => {
    this.props.onFilterQueryChange(this.searchNode.value)
  }

  render() {
    return (
      <div className={s.root}>
        <input
          className={s.input}
          ref={node => this.searchNode = node}
          onChange={this.onChange}
        />
      </div>
    );
  }

}

export default Search;