import React from 'react';
import * as pt from 'prop-types';
import s from './index.scss';

class User extends React.Component {
  static propTypes = {
    avatarUrl: pt.string.isRequired,
    name: pt.string.isRequired,
    id: pt.number.isRequired,
    email: pt.string.isRequired
  }

  render() {
    const {
      avatarUrl,
      name,
      id,
      email
    } = this.props;
    
    return (
      <div className={s.root}>
        <div className={s.avatar}>
          <img src={avatarUrl} />
        </div>
        <span className={s.name}>
          {this.props.name}
        </span>
      </div>
    )
  }

}

export default User;