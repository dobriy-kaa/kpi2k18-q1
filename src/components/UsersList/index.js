import React from 'react';
import * as pt from 'prop-types';
import Button from 'rambler-ui/Button';
import User from '../User';
import s from './index.scss';

class UsersList extends React.Component {
  static propTypes = {
    users: pt.arrayOf(pt.shape({
      avatarUrl: pt.string,
      name: pt.string,
      id: pt.number,
      email: pt.string
    })),
    loadMore: pt.func
  }

  static defaultProps = {
    users: [],
    loadMore: null
  }

  render() {
    const {
      users,
      loadMore
    } = this.props;
    return (
      <div className={s.root}>
        <div className={s.list}>
          {users.map(user => <User key={`user-${user.name}-${user.id}`} {...user} />)}
        </div>
        {loadMore && 
          <Button
            className={s.moreBtn} 
            onClick={loadMore}
          >
            Следующая страница
          </Button>
        }
      </div>
    );
  }

}

export default UsersList;